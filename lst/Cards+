# $Id: Cards+ 269992 2010-06-11 14:42:13Z anssi $
# This is the database of card definitions used by XFdrake
#

# Each definition should have a NAME entry, a DRIVER
#
# A reference to another definition is made with SEE (already defined
# entries are not overridden).
#
# Optional entries are:
#
# NOCLOCKPROBE: advises never to probe clocks
# UNSUPPORTED: indicates card that is not yet properly supported by XFree4
# LINE: adds a line of text to be included in the Device section (can include options or comments).
#
# DRI_GLX: 3D acceleration configuration for XFree 4 using DRI.
# DRI_GLX_EXPERIMENTAL: DRI, but EXPERIMENTAL and may freeze the machine.
#
# BAD_FB_RESTORE: for bad cards not restoring cleanly framebuffer (XFree 4)
#
# MULTI_HEAD 2: for DualHead cards (think Matrox G450)
# MULTI_HEAD n: for n Head cards (eg: "MULTI_HEAD 4" for QuadHead)
# FB_TVOUT: the card displays to a plugged TV when in framebuffer
#
#


######################################################################
# Generic drivers
NAME VESA driver (generic)
CHIPSET VESA VBE 2.0
DRIVER vesa

# Cirrus Logic

NAME Cirrus Logic GD542x-based cards
DRIVER vesa
LINE # Device section for Cirrus Logic GD5420/2/4/6/8/9-based cards.
LINE     #MemBase 0x00e00000
LINE     #MemBase 0x04e00000
LINE     #Option "linear"

NAME Cirrus Logic GD54xx-based cards
DRIVER cirrus

# S3 UniChrome (via)

# Some cards do not have 3D support, or 3D support is not safe: see
# http://wiki.openchrome.org/tikiwiki/tiki-index.php?page=HardwareCaveats
# According to pcpa, hardware cursor causes display corruption on some
# models as of 2007/08/21

NAME S3 UniChrome-based cards with 3D support
DRIVER openchrome
# 3D needs a DRM driver in kernel:
DRI_GLX
LINE Option	"SWcursor"

# Same as UniChrome, but no 3D support yet

NAME VIA Chrome9-based cards
DRIVER openchrome
LINE Option	"SWcursor"

# AMD

# GPUs supported by 'radeon' driver only
NAME AMD/ATI Radeon (radeon)
DRIVER radeon
FIRMWARE linux-firmware
DRIVER_NO_FIRMWARE vesa
DRI_GLX

# GPUs supported 'amdgpu' driver
NAME AMD Radeon (amdgpu)
DRIVER amdgpu
FIRMWARE linux-firmware
DRIVER_NO_FIRMWARE vesa
DRI_GLX

# Matrox

NAME Matrox Millennium / II / Productiva G100
DRIVER mga
BAD_FB_RESTORE

NAME Matrox Millennium G series (single head)
DRIVER mga
DRI_GLX
BAD_FB_RESTORE

NAME Matrox Millennium G series (dual head)
SEE Matrox Millennium G series (single head)
MULTI_HEAD 2

NAME Matrox Millennium G200 (quad head)
SEE Matrox Millennium G series (single head)
MULTI_HEAD 4

# NVIDIA

NAME NVIDIA GeForce, Quadro or NVS family (nouveau/nvidia304)
DRIVER nouveau
DRIVER2 nvidia304
DRIVER2_NEEDS_SSE

NAME NVIDIA GeForce, Quadro or NVS family (nouveau/nvidia340)
DRIVER nouveau
DRIVER2 nvidia340
DRIVER2_NEEDS_SSE

NAME NVIDIA GeForce, Quadro or NVS family (nouveau/nvidia390)
DRIVER nouveau
DRIVER2 nvidia390
DRIVER2_NEEDS_SSE

NAME NVIDIA GeForce, Quadro or NVS family (nouveau/nvidia410)
DRIVER nouveau
DRIVER2 nvidia410
DRIVER2_NEEDS_SSE

NAME NVIDIA GeForce, Quadro or NVS family (nouveau/nvidia460)
DRIVER nouveau
DRIVER2 nvidia460
DRIVER2_NEEDS_SSE

# Intel

NAME Intel 810 and later
DRIVER intel
DRI_GLX

NAME Intel Vermilion-based cards
DRIVER vesa

NAME Intel Poulsbo US15W (GMA500)
DRIVER modesetting
DRIVER2 vesa

# QXL

# QXL virtual video card
NAME QXL virtual video card
DRIVER qxl

# VMware virtual video cards

NAME VMware virtual video card
DRIVER vmware

# VirtualBox virtual video cards

NAME VirtualBox virtual video card
DRIVER vboxvideo

# Misc

END
