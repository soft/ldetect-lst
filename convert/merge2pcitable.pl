#!/usr/bin/perl

use MDK::Common;

#use warnings;

my @ignored_modules = (
qw(alsa ignore),
);

my ($force, @force_modules, $all, $keep_subids, $wildcards, $use_description);

# UPDATE ME WHEN UPDATING ../lst/Cards+:
# ATI/AMD
my $ati_driver_legacy  = 'Card:ATI Radeon HD 2000 to HD 4870 (radeon/fglrx-legacy)';
my $ati_driver_fw   = 'Card:ATI Radeon HD 6400 and later (radeon/fglrx)';
my $ati_driver_vesa = 'Card:ATI Radeon HD 5000 and later without free driver (vesa/fglrx)';
my $ati_free_only   = 'Card:ATI Radeon HD 4870 and earlier (radeon only)';
# NVidia - "Card:NVIDIA GeForce, Quadro or NVS family (nouveau/<proprietary_driver_name>)"
# 
# also, be careful when merging as Cards+ and pcitable may contain card-specific
# cases due to bugs in the various drivers
my $vesa_generic = 'Card:VESA driver (generic)';

my $usage =
"Usage:
    $0 [-f[=module1,...]] [-a] [--old] <format> <in_file> <pcitable>
or
    $0 nvidia_reset <pcitable>\n";

if ($0 =~ /merge2pcitable/) 
{
    if ($ARGV[0] =~ /^-f=?(.*)$/) {
        shift;
        @force_modules = split(/,/, $1);
        $force = !@force_modules;
    }
    $ARGV[0] eq '-a' and $all = shift;
    $ARGV[0] eq '--replace' and $if_replace = shift;
    $ARGV[0] eq '--keep-subids' and $keep_subids = shift;
    $ARGV[0] eq '--handle-wildcards' and $wildcards = shift;
    $ARGV[0] eq '--old' and $use_description = shift;

    my $formats = join '|', grep { $_ } map { /^read_(.*)/ ? $1 : '' } keys %main::;
    
    # 'nvidia_reset' needs only 2 command line arguments (incl. itself).
    my ($format, $in, $pcitable);
    if (@ARGV == 2) {
        ($format, $pcitable) = @ARGV;
        $format eq 'nvidia_reset' or die $usage;
    }
    else {
        @ARGV == 3 or die $usage;
        ($format, $in, $pcitable) = @ARGV;
    }

    my $d_pci = read_pcitable($pcitable, 'strict');
    my $d_in;
    my $classes;
    
    # Special cases for NVidia graphics drivers.
    if ($format eq 'nvidia_reset') {
        ($d_in, $classes) = nvidia_reset($d_pci);
    }
    elsif ($format =~ /^(nvidia\d+)_readme/) {
        ($d_in, $classes) = read_nvidia($in, $1);
    }
    else {
        # A subroutine named read_$format() will be called with $in as an 
        # argument.
        my $read = $main::{"read_$format"} or die "unknown format $format (must be one of $formats)\n";
        ($d_in, $classes) = $read->($in);
    }

    #ATI/AMD hacks for support/unsupport device
    # First pass: detecting legacy devices. Going through the list of all known devices and drop their drivers
    # to legacy (if the device is in the legacy list), or to free driver (will uplift it on further passes).
    # Add new devices as legacy as well.
    if ($format eq 'fglrxko_pci_ids_h_legacy') {
        foreach (keys %$d_pci) {
            if (member($d_pci->{$_}[0], ($ati_driver_legacy, $ati_driver_fw, $ati_driver_vesa))) {
                $d_pci->{$_}[0] = ($d_in->{$_} ? $ati_driver_legacy : $ati_free_only);
            }
        }

        foreach (keys %$d_in) {
            if (!$d_pci->{$_}) {
                $d_pci->{$_}[0] = $ati_driver_legacy;
            }
        }
    }

    # Second pass: uplifting drivers for supported devices.
    # 1) For supported devices set the driver to vesa/fglrx.
    # 2) For unsupported devices (which was been a vesa/fglrx only
    #    and not support by legacy driver) set the driver to free.
    # 3) For new devices set the driver to vesa/fglrx.
    if ($format eq 'fglrxko_pci_ids_h') {
        foreach (keys %$d_pci) {
            if (member($d_pci->{$_}[0], ($ati_driver_legacy, $ati_driver_fw)) && $d_in->{$_}) {
#                $d_pci->{$_}[0] = ($d_in->{$_} ? $ati_driver_vesa : $ati_free_only);
                # support dropped, handle:
                if ($d_pci->{$_}[0] eq $ati_driver_legacy || $d_pci->{$_}[0] eq $ati_driver_fw) {
                    $d_pci->{$_}[0] = $ati_driver_vesa;
                } 
             } elsif ($d_pci->{$_}[0] eq $ati_driver_vesa && !$d_in->{$_}) {
                    $d_pci->{$_}[0] = $ati_driver_free;
                }
            }
        foreach (keys %$d_in) {
            if (!$d_pci->{$_}) {
                $d_pci->{$_}[0] = $ati_driver_vesa;
            }
        }
    }
    # Third pass: uplifting to radeon drivers.
    # 1) For supported devices set the driver to radeon/fglrx.
    # 2) For unsupported devices with free drivers set the driver to vesa.
    # 3) For new devices set the driver to free.
    if ($format eq 'ati_pciids_csv') {
        foreach (keys %$d_pci) {
            if ($d_pci->{$_}[0] eq $ati_driver_vesa) {
                if ($d_in->{$_}) {
                    $d_pci->{$_}[0] = $ati_driver_fw;
                }
            }
            elsif ($d_pci->{$_}[0] eq $ati_free_only) {
                if (!$d_in->{$_}) {
                    $d_pci->{$_}[0] = $vesa_generic;
                }
            }
        }
        foreach (keys %$d_in) {
            if (!$d_pci->{$_}) {
                $d_pci->{$_}[0] = $ati_free_only;
            }
        }
    }
    
    # Handling of NVidia GPUs. The data about which ones are supported 
    # should be reset first by calling this script with 'nvidia_reset' 
    # argument instead of the format.
    # Then, this script should be called again for each long-lived driver
    # branch (starting from the oldest supported one) with appropriate 
    # Readme file from the driver package. Thus, for each supported GPU, the
    # newest long-lived driver will be specified in the pcitable as a result.
    # 
    # Here is the part that actually adds infotmation about which devices 
    # are supported to the table.
    if ($format =~ /^(nvidia\d+)_readme/) {
        foreach (keys %$d_in) {
            $d_pci->{$_}[0] = $d_in->{$_}[0];
        }
    }

    merge($d_pci, $d_in, $classes);
    exit 1 if our $error;
    cleanup_subids($d_pci) if !$keep_subids;
    
    write_pcitable($d_pci);
} else { 1 }

sub dummy_module { 
    my ($m) = @_;
    $m =~ s/"(.*)"/$1/;
    member($m, @ignored_modules);
}

sub to_string {
    my ($id, $driver) = @_;
    @$driver >= 1 or error("error: to_string $id");
    my ($module, $text) = map { defined($_) && qq("$_") } @$driver;
    my ($id1, $id2, $subid1, $subid2) = map { "0x$_" } ($id =~ /(....)/g);
    join "\t", $id1, $id2, if_("$subid1 $subid2" ne "0xffff 0xffff", $subid1, $subid2), $module, if_($use_description && $text, $text);
}

sub read_rhpcitable {
    my ($f, $strict) = @_;
    read_pcitable($f, $strict, 1);
}

# works for RedHat's pcitable old and new format, + mdk format (alike RedHat's old one)
# (the new format has ending .o's and double quotes are removed)
sub read_pcitable {
    my ($f, $strict, $o_newer_rh_format) = @_;
    my %drivers;
    my %class;
    my $line = 0;
    my $rm_quote_silent = sub { local ($_) = @_; s/^"//; s/"$//; $_ };
    my $rm_quote = sub {
            local ($_) = @_; 
            s/^"// or error("$f:$line: missing left quote");
            s/"$// or error("$f:$line: missing right quote");
            /"/ && $strict and error("$f:$line: bad double quote");
            $_;
    };
    foreach (eval { catMaybeCompressed($f) }) {
        chomp; $line++;
        next if /^#/ || /^\s*$/;

        if (!$strict) {
            #- help poor written pcitable's like redhat's :)
            s/(\S+)\s+(\S+)\s+(.*)/$1\t$2\t$3/;
        }

        if (my ($id1, $id2, @l) = split /\t+/) {
            push @l, '""' if $o_newer_rh_format;
            my ($subid1, $subid2) = ('ffff', 'ffff');
            ($subid1, $subid2, @l) = @l if @l > 2;
            @l == 1 || @l == 2 or die "$f:$line: bad number of fields " . (int @l) . " (in $_)\n";
            my ($module, $text) = @l;

            my $class = $text =~ /(.*?)|/;
            my $id1_ = $rm_quote_silent->($id1);
            if (defined $text && $class{$id1_}) {
                print STDERR "$f:$line: class $id1_ named both $class and $class{$id1_}, taking $class{$id1_}\n";
                $class{$id1_} ||= $1;
                $text =~ s/(.*?)|/$class{$id1_}|/;
            }

            $module =~ s/\.o$//;
            $module = '"unknown"' if dummy_module($module);
            $module = '"unknown"' if $id1 eq '0x1011' && $id2 eq '0x0004';
              # known errors in redhat's pcitable
              # these are pci to pci bridge
            $module = '"yenta_socket"' if $module =~ /i82365/;
            my $id = join '', map { 
                s/^0x//;
                length == 4 or error("$f:$line: bad number $_");
                lc($_);
            } $id1, $id2, $subid1, $subid2;
            $drivers{$id} && $strict and error("$f:$line: multiple entry for $id (skipping $module $text)");
            $drivers{$id} ||= [ $rm_quote->($module), defined($text) ? $rm_quote->($text) : undef, $line ];
        } else {
            die "$f:$line: bad line\n";
        }
    }

    \%drivers;
}

sub read_kernel_aliasmap_pci {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
                next if !/alias pci/;
                        if (/alias pci:v0000(....)d0000(....)sv/) {
                                my $module = (split)[2];
                                my ($id1, $id2) = (lc($1), lc($2));
                                $drivers{ join '', map { /(....)$/ } $id1, $id2, '0xffff', '0xffff' } = [ $module, '' ];
                        }
                        if (/alias pci:v0000(....)d0000(....)d0000(....)d0000(....)bc/) {
                                my $module = (split)[2];
                                my ($id1, $id2, $subid1, $subid2) = (lc($1), lc($2), lc($3), lc($4));
                                $drivers{ join '', map { /(....)$/ } $id1, $id2, $subid1, $subid2 } = [ $module, '' ];
                        }
    }
    \%drivers;
}

#read usb device map from modules.aliace (kmod related)
sub read_kernel_aliasmap_usb {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
                next if !/alias usb/;
                if (/alias usb:v(....)p(....)d/) {
                        my $module = (split)[2];
                        my ($id1, $id2) = (lc($1), lc($2));
                        $drivers{join '', map { /(....)$/ } $id1, $id2, "ffff", "ffff"} = [ $module, '' ];
                }
    }
    \%drivers;
}

sub read_kernel_aliasmap_fglrx {
    my ($f) = @_;
    my %drivers;
    if (!$f || $f eq '/dev/null') {
        $f = find { -e $_ } map { "$_/dkms-modules-info/dkms-modules.alias" } qw(. ..);
    }
    foreach (cat_($f)) {
        # too bad nvidia driver doesn't list its ids...
        next if !/alias pci.* fglrx/;
        if (/alias pci:v0000(....)d0000(....)sv/) {
            my ($id1, $id2) = (lc($1), lc($2));
            $drivers{ join '', map { /(....)$/ } $id1, $id2, '0xffff', '0xffff' } = [ $ati_driver_vesa ];
        }
    }
    \%drivers;
}

sub read_kernel_pcimap {
    my ($f) = @_;
    my (%drivers, %driver_with_classes);
    foreach (cat_($f)) {
        chomp;
        next if /^#/ || /^\s*$/;
        my ($module, $id1, $id2, $subid1, $subid2) = split;
        next if $module eq 'pci';
        ($subid1, $subid2) = ("ffff", "ffff") if hex($subid1) == 0 && hex($subid2) == 0;
     if ($id2 =~ /ffff$/ && $id1 !~ /ffff$/) {
         # $driver_with_classes{$id1} = [ $module, '' ];
         $driver_with_classes{join '', map { /(....)$/ } $id1, $id2, $subid1, $subid2} = [ $module, '' ];
     } else {
         $drivers{join '', map { /(....)$/ } $id1, $id2, $subid1, $subid2} = [ $module, '' ];
     }
    }
    \%drivers, \%driver_with_classes;
}

sub read_kernel_usbmap {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        chomp;
        next if /^#/ || /^\s*$/;
        my ($module, $flag, $id1, $id2) = split;
        hex($flag) == 3 or next;
        $drivers{join '', map { /(....)$/ } $id1, $id2, "ffff", "ffff"} = [ $module, '' ];
    }
    \%drivers;
}

sub read_pciids {
    my ($f) = @_;
    my %drivers;
    my ($id1, $id2, $class, $line, %class);
    foreach (cat_($f)) {
        chomp; $line++;
        next if /^#/ || /^;/ || /^\s*$/;
        if (/^C\s/) {
            last;
        } elsif (my ($subid1, $subid2, $text) = /^\t\t(\S+)\s+(\S+)\s+(.+)/) {
            $text =~ s/\t/ /g;
            $id1 && $id2 or die "$f:$line: unexpected device\n";
            $drivers{sprintf qq(%04x%04x%04x%04x), hex($id1), hex($id2), hex($subid1), hex($subid2)} = [ "unknown", "$class|$text" ];
        } elsif (/^\t(\S+)\s+(.+)/) {
            ($id2, $text) = ($1, $2);
            $text =~ s/\t/ /g;
            $id1 && $id2 or die "$f:$line: unexpected device\n";
            $drivers{sprintf qq(%04x%04xffffffff), hex($id1), hex($id2)} = [ "unknown", "$class|$text" ];
        } elsif (/^(\S+)\s+(.+)/) {
            $id1 = $1;
            $class = $class{$2} || $2;
            $class =~ s/(Advanced Micro Devices) \[AMD\]/$1/;
        } else {
            warn "bad line: $_\n";
        }
    }
    \%drivers;
}

sub read_pcilst {
    my ($f) = @_;
    my %drivers;
    my ($class, $line, %class);
    foreach (cat_($f)) {
        chomp; $line++;
        next if /^#/ || /^;/ || /^\s*$/;
        if (/^\t\S/) {
            my ($id, undef, $module, $text) = split ' ', $_, 4 or die "bad line: $_\n";
            $text =~ s/\t/ /g;
            $module = "unknown" if dummy_module($module);
            $drivers{"${id}ffffffff"} = [ $module, "$class|$text" ];
        } elsif (/^(\S+)\s+(.*)/) {
            $class = $class{$2} || $2;
        } else {
            die "bad line: $_\n";
        }
    }
    \%drivers;
}

sub read_pcitablepm {
    my ($f) = @_;
    eval cat_($f);
    my %drivers;

    %pci_probing::pcitable::ids or die;
    while (my ($k, $v) = each %pci_probing::pcitable::ids) {
        $drivers{sprintf qq(%08xffffffff), $k >> 32} = [ $v->[1], $v->[0] ];
    }
    \%drivers;
}

sub read_hwd {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        next if /^\s*#/;
        chomp;
        my ($id1, $id2, $_class, $module, $_undef, $descr) = /(....):(....)\s+(\S+)\s+(\S+)(\s+(.*))/ or next;
        $drivers{"$id1${id2}ffffffff"} = [ $module, $descr ];
    }
    \%drivers;
}

sub read_hwinfo_x11 {
    my ($f) = @_;

    my (%drivers, %e, %vendors, $line);
    foreach (cat_($f)) {
        $line++;
        s/\s*$//;
        if (my ($add, $name, $val) = /^([ &])(\w+)\.id\s+(.*)/) {
            if (!$add) {
                warn "read_hwinfo_x11:$line: unused %e\n" if %e;
                %e = ();
            }
            if ($val =~ /^pci\s+0x([0-9a-f]{4})/i) {
                $val = hex $1;
            } else {
                warn "read_hwinfo_x11:$line: weird value $val\n";
            }
            $e{$name} = $val;
        } elsif (/^\+vendor\.name\s+(.*)/) {
            $vendors{$e{vendor}} = $1;
        } elsif (/^\+(?:sub)?device\.name\s+(.*)/) {
            $e{name} = $1;
        } elsif (my ($driver) = /^\+driver\.xfree\s+(.*)/) {
            if (exists $e{vendor} && exists $e{device}) {
                my $vendor = $vendors{$e{vendor}};
                my $module = $driver =~ /^4\|(\w+)/ ? "Driver:$1" : "Card:$driver";
                $drivers{sprintf qq(%04x%04x%04x%04x), $e{vendor}, $e{device}, 
                         $e{subvendor} || 0xffff, $e{subdevice} || 0xffff} = [ $module, "$vendor|$e{name}" ];
            } else {
                warn "read_hwinfo_x11:$line: $driver but no vendor or no device\n";
            }
        } elsif (/^$/) {
            %e = ();
        } elsif (/^\+driver\.xfree\.config/) {
            # drop
        } else {
            warn "read_hwinfo_x11:$line: unknown line $_\n";
        }
    }
    \%drivers;
}

# Reset data for NVidia graphics cards: drop information about the drivers
# to handle them.
sub nvidia_reset {
    my ($d_pci) = @_;
    foreach (keys %$d_pci) {
        if ($d_pci->{$_}[0] =~ /NVIDIA_UNKNOWN|NVIDIA GeForce/) {
            $d_pci->{$_}[0] = $vesa_generic;
        }
    }
}

# Read the Readme file (the 1st argument) from the driver package and 
# extract information about the supported GPUs.
# The second argument is the id of the driver (nvidia304, nvidia340, etc.)
#
# The relevant parts of the Readme are expected to have the following format:
#
# A1. NVIDIA GEFORCE GPUS
# <one or more blank lines>
#   NVIDIA GPU product       Device PCI ID
#   ------------------------ --------------
#   GeForce 6800 Ultra       0x0040         <may be smth else>
#   GeForce 6800             0x0041 0x5574  <may be smth else>
#   GeForce 6800 LE          0x0042         <may be smth else>
#   GeForce 6800 XE          0x0043         <may be smth else>
# <... since 346.x the format of the lines has changed: >
#   GeForce GTX 660 Ti       1183           <may be smth else>
#   GeForce GT 620M          1140 1B0A 20DD <may be smth else>
# <...>
# <one or more blank lines>
sub read_nvidia {
    my ($f, $driver) = @_;
    my $card = "NVIDIA GeForce, Quadro or NVS family (nouveau/$driver)";
    my %drivers;
    my $in_section = 0;
    my $in_body = 0;

    foreach (cat_($f)) {
        chomp;
        
        # Process NVidia GeForce, Quadro and NVS only. Ignore Tesla & GRID
        # here because these are not intended for graphics output.
        $in_section = 1 if /NVIDIA (GEFORCE|QUADRO|RTX\/QUADRO|NVS) GPUS/;
        next unless $in_section;
        
        if (/^\s+NVIDIA GPU product\s+Device PCI ID/) {
            $in_body = 1;
            next;
        }
        next unless $in_body;
        
        # Each section ends with one or more blank lines.
        if (/^\s*$/) {
            $in_section = 0;
            $in_body = 0;
            next;
        }
        
        # Skip the separator.
        next if /^\s+-+[\s-]+$/;

        # Shorten non-important strings and pad them with 4+ spaces to make
        # parsing easier.
        s/with Max-Q Design/(Max-Q)    /;

        if (/^\s+(.+?)\s{4,}(?:0x)?([[:xdigit:]]{4})(?: (?:0x)?([[:xdigit:]]{4}))?(?: (?:0x)?([[:xdigit:]]{4}))?/) {
            my $description = $1;
            my $id = $2;
            my $subvendor;
            my $subid;
            my $vendor_nvidia = "10de";
            
            if ($4) {
                $subvendor = $3 ? $3 : $vendor_nvidia;
                $subid = $4;
            } else {
                $subid = $3;
                $subvendor = $subid ? $vendor_nvidia : "";
            }
            
            $id = $vendor_nvidia . lc($id);
            $subid = $subid ? lc($subvendor) . lc($subid) : "ffffffff";
            $drivers{$id . $subid} = [ "Card:$card", $description ];
        }
        else {
            print STDERR "Invalid line in the input file:\n$_\n";
            next;
        }
    }

    \%drivers;
}

sub read_fglrxko_pci_ids_h {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        chomp;
        my ($id) = /^\s+FGL_ASIC_ID\(0x(....)\)/ or next;
        $drivers{"1002" . lc($id) . "ffffffff"} = [ $ati_driver_vesa, 'unknown' ];
    }
    \%drivers;
}

sub read_fglrxko_pci_ids_h_legacy {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        chomp;
        my ($id) = /^\s+FGL_ASIC_ID\(0x(....)\)/ or next;
        $drivers{"1002" . lc($id) . "ffffffff"} = [ $ati_driver_legacy, 'unknown' ];
    }
    \%drivers;
}

sub read_rhd_id_c {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        chomp;
        my ($id, $description) = m!^\s+RHD_DEVICE_MATCH\(\s*0x(....).*/\* (.*)\*/! or next;
        $drivers{"1002" . lc($id) . "ffffffff"} = [ "Card:RADEONHD_FIXME", $description ];
    }
    \%drivers;
}

sub read_ati_pciids_csv {
    my ($f) = @_;
    my %drivers;
    foreach (cat_($f)) {
        chomp;
        my ($id, $description) = /^"0x(....)",.*,(?:"([^,]*)")?$/ or next;
        $drivers{"1002" . lc($id) . "ffffffff"} = [ $ati_free_only, $description ];
    }
    \%drivers;
}

# write in RedHat's pcitable old format (mdk one)
sub write_pcitable {
    my ($drivers) = @_;
    foreach (sort keys %$drivers) {
        print to_string($_, $drivers->{$_}), "\n";
    }
}


sub merge_entries_with_wildcards {
    my ($drivers, $classes) = @_;
    foreach (keys %$classes) {
        my ($vendor, $id, $subvendor, $subid);
        next unless ($vendor, $id, $subvendor, $subid) = /^([0-9a-f]{4,4})([0-9a-f]{4,4})/;

        # handle PCI_ANY_ID as PCI device ID:
        if ($vendor !~ /ffff$/ && $id =~ /ffff$/) {
            foreach my $old (keys %$drivers) {
                next if $old !~ /^$vendor/ || $drivers->{$old}[0] ne 'unknown';
                # blacklist AGP for now;
                next if $classes->{$_}[0] =~ /agp/;
                # the following test would be better but still generates some wrong entries (the only real check is to check
                # PCI_CAP_ID_AGP at probing time):
                # next if $classes->{$_}[0] =~ /-agp/ && $drivers->{$old}[1] !~ /Bridge|Controller|Host/i;
                $drivers->{$old}[0] = $classes->{$_}[0]; # if $drivers->{$old}[0] eq "unknown";
            }
        }
     }
}

sub merge {
    my ($drivers, $new, $classes) = @_;
    merge_entries_with_wildcards($drivers, $classes) if $wildcards;

    foreach (keys %$new) {
        next if $new->{$_}[0] =~ /parport_pc|i810_ng/;
        if ($drivers->{$_}) {
            if ($new->{$_}[0] ne "unknown") {
                if ($drivers->{$_}[0] eq "unknown" || $force || member($new->{$_}[0], @force_modules)) {
                    $drivers->{$_}[0] = $new->{$_}[0];
                } elsif ($if_replace) { 
                    $drivers->{$_}[0] = $new->{$_}[0];
                } elsif ($drivers->{$_}[0] ne $new->{$_}[0]) {
                    my $different = 1;
                    $different = 0 if $new->{$_}[0] =~ /fb/;
                    $different = 0 if $drivers->{$_}[0] =~ /^(Card|Server):/;
                    $different = 0 if $drivers->{$_}[0] =~ /^ISDN:([^,]+)/ && $new->{$_}[0] eq $1;
                    print STDERR "different($drivers->{$_}[0] $new->{$_}[0]): ", to_string($_, $drivers->{$_}), "\n" if $different;
                }
            }
            next if !$new->{$_}[1];
            $drivers->{$_}[1] = $new->{$_}[1] if !$drivers->{$_}[1] || $drivers->{$_}[1] =~ /\|$/;
        } else {
            if (!/ffffffff$/ && $new->{$_}[0] eq "unknown") {
                # keep sub-entry with major-entry module
                # will be dropped if every subids have the same module
                # ie. if no subids already present have a different module than the main one
                if (/(........)/) {
              $new->{$_}[0] = $drivers->{$1 . 'ffffffff'}[0] || "unknown" 
                if exists $drivers->{$1 . 'ffffffff'};
          }
            }

            $drivers->{$_} = $new->{$_} 
              # don't keep sub-entries with unknown drivers
              if $all || /ffffffff$/ || $new->{$_}[0] ne "unknown";
        }        
    }
}

sub cleanup_subids {
    my ($drivers) = @_;
    my (%l, %m);
    foreach (sort keys %$drivers) {
        my ($id, $subid) = /(........)(........)/;
        if ($l{$id}) {
            push @{$m{$id}}, $l{$id}, $subid;
        } else {
            $l{$id} = $subid;
        }
    }
    foreach my $id (keys %m) {
        my %modules;
        my $text = "";
        foreach my $subid (@{$m{$id}}) {
            my $e = $drivers->{"$id$subid"};
            $modules{$e->[0]} = 1;
            $text = $e->[1] if length($e->[1]) > length($text) || $subid eq 'ffffffff'; # favour previous text
        }
        if (keys(%modules) == 1) {
            my ($module, undef) = each %modules;
                                    
            # remove others
            foreach my $subid (@{$m{$id}}) {
                delete $drivers->{"$id$subid"};                
            }
            # add a main one
            $drivers->{$id . 'ffffffff'} = [ $module, $text ];
        }
    }
}

sub error {
    our $error = 1;
    print STDERR "$_[0]\n";
}
