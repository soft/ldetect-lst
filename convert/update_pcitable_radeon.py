#!/usr/bin/env python3

############################################################################
# Add/update the entries for AMD/ATI Radeon GPUs in the PCI ID table with
# those from the output of modinfo (<modinfo_file>) for the free drivers,
# radeon and amdgpu.
#
# The previous entries for such GPUs are removed.
#
# Usage:
#   update_pcitable_radeon.py <modinfo_file_radeon> <modinfo_file_amdgpu> \
#       < old_pcitable > new_pcitable
############################################################################

import sys
import re

RADEON_STRING_RE = re.compile(r'Card:(ATI|AMD).*Radeon.*(radeon|fglrx|amdgpu)')

ALIAS_RE = re.compile(
    r'\s*alias:\s*pci:v0000(.{4})d0000(.{4})sv([*]|.{8})sd([*]|.{8})')

RADEON_VENDOR = '0x1002'


def load_pcitable(file_from):
    '''Load the PCI IDs table from the given file object

    Returns dict (IDs => name).
    '''
    table = {}
    for rec in file_from:
        rec = rec.strip()
        if rec == '':
            continue
        parts = rec.split('\t')
        # 'vendor_id device_id name' or
        # 'vendor_id device_id subvendor_id subdevice_id name'
        if len(parts) == 3:
            dev = (parts[0], parts[1])
            name = parts[2]
        elif len(parts) == 5:
            dev = (parts[0], parts[1], parts[2], parts[3])
            name = parts[4]
        else:
            raise Exception('Invalid line: \'%s\'\n' % rec)

        # Filter out the old entries for Radeon GPUs
        if not RADEON_STRING_RE.search(name):
            table[dev] = name

    return table

def load_supported_ids(modinfo_file):
    '''Load the PCI IDs of the supported devices from the given file object

    The file object should correspond to the output of modinfo for the
    appropriate kernel module.

    Returns the set of PCI ID tuples extracted from that file.'''
    supported = set()
    for rec in modinfo_file:
        m = ALIAS_RE.match(rec)
        if not m:
            continue

        vendor_id = '0x' + m.group(1).lower()
        if vendor_id != RADEON_VENDOR:
            raise Exception('Invalid vendor in alias line: \"%s\"' % rec)

        device_id = '0x' + m.group(2).lower()

        subvendor_id = None
        sv = m.group(3).lower()
        if sv != '*':
            if sv.startswith('0000'):
                sv = sv[4:]
            subvendor_id = '0x' + sv

        subdevice_id = None
        sd = m.group(4).lower()
        if sd != '*':
            if sd.startswith('0000'):
                sd = sd[4:]
            subdevice_id = '0x' + sd
            if not subvendor_id:
                subvendor_id = RADEON_VENDOR

        if subdevice_id:
            supported.add(
                (vendor_id, device_id, subvendor_id, subdevice_id))
        else:
            supported.add((vendor_id, device_id))

    return supported

def print_record(ids, name):
    for i in ids:
        sys.stdout.write(i + '\t')
    sys.stdout.write(name + '\n')

############################################################################

CARD_AMDGPU = '\"Card:AMD Radeon (amdgpu)\"'

# Some older (ATI-manufactured) cards may be serviced by 'radeon' driver
# too, hence "AMD/ATI" here.
CARD_RADEON = '\"Card:AMD/ATI Radeon (radeon)\"'

# main
if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.stderr.write('Usage:\npython3 ' + sys.argv[0] + ' ')
        sys.stderr.write('<modinfo_file_radeon> <modinfo_file_amdgpu> ')
        sys.stderr.write('< old_pcitable > new_pcitable\n')
        sys.exit(1)

    table = load_pcitable(sys.stdin)

    with open(sys.argv[1], 'r') as modinfo_file:
        ids_radeon = load_supported_ids(modinfo_file)
    sys.stderr.write('Supported by radeon: %d GPUs\n' % len(ids_radeon))

    with open(sys.argv[2], 'r') as modinfo_file:
        ids_amdgpu = load_supported_ids(modinfo_file)
    sys.stderr.write('Supported by amdgpu: %d GPUs\n' % len(ids_amdgpu))

    # Remove the entries from the table that correspond to the GPUs
    # supported by either radeon or amdgpu (or both). Although they may have
    # been filtered out already by load_pcitable(), some may still remain if
    # they have incorrect value, for example.
    for dev in ids_radeon:
        if dev in table:
            del table[dev]
    for dev in ids_amdgpu:
        if dev in table:
            del table[dev]

    for dev in ids_radeon:
        table[dev] = CARD_RADEON

    # If the device is supported by the both drivers, suggest amdgpu.
    for dev in ids_amdgpu:
        table[dev] = CARD_AMDGPU

    for dev in sorted(table.keys()):
        print_record(dev, table[dev])
